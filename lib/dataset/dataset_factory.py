from lib.dataset import voc
from lib.dataset import coco

dataset_map = {
                'voc': voc.VOCDetection,
                'coco': coco.COCODetection,
            }

def gen_dataset_fn(name):
    """Returns a dataset func.

    Args:
    name: The name of the dataset.

    Returns:
    func: dataset_fn

    Raises:
    ValueError: If network `name` is not recognized.
    """
    if name not in dataset_map:
        raise ValueError('The dataset unknown %s' % name)
    func = dataset_map[name]
    return func


import torch
import numpy as np
import cv2

target_sizes=[]
max_size=0

def im_list_to_blob(ims,targets):
    """Convert a list of images into a network input. Assumes images were
    prepared using prep_im_for_blob or equivalent: i.e.
      - BGR channel order
      - pixel means subtracted
      - resized to the desired input size
      - float32 numpy ndarray format
    Output is a 4D HCHW tensor of the images concatenated along axis 0 with
    shape.
    """
    max_shape = np.array([im.shape for im in ims]).max(axis=0)
    # Pad the image so they can be divisible by a stride
    # if cfg.FPN.FPN_ON:
    FPN_ON = True
    COARSEST_STRIDE = 32
    if FPN_ON:
        # stride = float(cfg.FPN.COARSEST_STRIDE)
        stride = float(COARSEST_STRIDE)
        max_shape[0] = int(np.ceil(max_shape[0] / stride) * stride)
        max_shape[1] = int(np.ceil(max_shape[1] / stride) * stride)

    num_images = len(ims)
    blob = np.zeros((num_images, max_shape[0], max_shape[1], 3),
                    dtype=np.float32)
    for i in range(num_images):
        im = ims[i]
        blob[i, 0:im.shape[0], 0:im.shape[1], :] = im
        # print (targets[i],len(targets[i]))
        num=len(targets[i])
        # if num==1:
        #     targets[i][0] = np.float32((targets[i][0] * im.shape[1]) / max_shape[1])
        #     targets[i][2] = np.float32((targets[i][2] * im.shape[1]) / max_shape[1])
        #     targets[i][1] = np.float32((targets[i][1] * im.shape[0]) / max_shape[0])
        #     targets[i][3] = np.float32((targets[i][3] * im.shape[0]) / max_shape[0])
        # elif num>1:
        #     for j in range(num):
        #         targets[i][j][0] = np.float32((targets[i][j][0] * im.shape[1]) / max_shape[1])
        #         targets[i][j][2] = np.float32((targets[i][j][2] * im.shape[1]) / max_shape[1])
        #         targets[i][j][1] = np.float32((targets[i][j][1] * im.shape[0]) / max_shape[0])
        #         targets[i][j][3] = np.float32((targets[i][j][3] * im.shape[0]) / max_shape[0])

        # if num==1:
        #     targets[i][0] = (targets[i][0] * im.shape[1]) / max_shape[1]
        #     targets[i][2] = (targets[i][2] * im.shape[1]) / max_shape[1]
        #     targets[i][1] = (targets[i][1] * im.shape[0]) / max_shape[0]
        #     targets[i][3] = (targets[i][3] * im.shape[0]) / max_shape[0]
        # elif num>1:
        #     for j in range(num):
        #         targets[i][j][0] = (targets[i][j][0] * im.shape[1]) / max_shape[1]
        #         targets[i][j][2] = (targets[i][j][2] * im.shape[1]) / max_shape[1]
        #         targets[i][j][1] = (targets[i][j][1] * im.shape[0]) / max_shape[0]
        #         targets[i][j][3] = (targets[i][j][3] * im.shape[0]) / max_shape[0]
        w=float(im.shape[1])
        h=float(im.shape[0])
        max_1=float(max_shape[1])
        max_0=float(max_shape[0])
        # if num==1:
        #     # print ('targets[i]',targets[i])
        #     # print ('targets[i][3]',type(targets[i][3]))
        #     targets[i][0] = (targets[i][0] * w) / max_1
        #     targets[i][2] = (targets[i][2] * w) / max_1
        #     targets[i][1] = (targets[i][1] * h) / max_0
        #     targets[i][3] = (targets[i][3] * h) / max_0

        # elif num>1:
        for j in range(num):
                # print ('targets[i][j][3]',type(targets[i][j][3]))
                targets[i][j][0] = (targets[i][j][0] * w) / max_1
                targets[i][j][2] = (targets[i][j][2] * w) / max_1
                targets[i][j][1] = (targets[i][j][1] * h) / max_0
                targets[i][j][3] = (targets[i][j][3] * h) / max_0

    # Move channels (axis 3) to axis 1
    # Axis order will become: (batch elem, channel, height, width)
    channel_swap = (0, 3, 1, 2)
    blob = blob.transpose(channel_swap)
    return blob,targets


def prep_im_for_blob(im, target_sizes, max_size):
    """Prepare an image for use as a network input blob. Specially:
      - Subtract per-channel pixel mean
      - Convert to float32
      - Rescale to each of the specified target size (capped at max_size)
    Returns a list of transformed images, one for each target size. Also returns
    the scale factors that were used to compute each returned image.
    """
    # im = preprocess(im, method=method)

    im_shape = im.shape
    im_size_min = np.min(im_shape[0:2])
    im_size_max = np.max(im_shape[0:2])

    ims = []
    im_scales = []
    for target_size in target_sizes:
        im_scale = float(target_size) / float(im_size_min)
        # Prevent the biggest axis from being more than max_size
        if np.round(im_scale * im_size_max) > max_size:
            im_scale = float(max_size) / float(im_size_max)
        im = cv2.resize(im, None, None, fx=im_scale, fy=im_scale,
                        interpolation=cv2.INTER_LINEAR)
        ims.append(im)
        im_scales.append(im_scale)
    return ims, im_scales

def detection_collate(batch):
    """Custom collate fn for dealing with batches of images that have a different
    number of associated object annotations (bounding boxes).

    Arguments:
        batch: (tuple) A tuple of tensor images and lists of annotations

    Return:
        A tuple containing:
            1) (tensor) batch of images stacked on their 0 dim
            2) (list of tensors) annotations for a given image are stacked on 0 dim
    """
    targets = []
    imgs = []

    for _, sample in enumerate(batch):
        # print ("sample",sample)
        for _, tup in enumerate(sample):
            # print ('tup',tup)
            if torch.is_tensor(tup):
                imgs.append(tup)
                # print (imgs)
            elif isinstance(tup, type(np.empty(0))):
                annos = torch.from_numpy(tup).float()
                targets.append(annos)
    # print(target_sizes,max_size)


  
    processed_ims = []
    im_scales = []
    for im in imgs:
        # print ('im',im)
        im=im.numpy()
        im, im_scale = prep_im_for_blob(im, target_sizes,max_size)  #[480], 640
        im_scales.append(im_scale[0])
        processed_ims.append(im[0])
    imgs,targets = im_list_to_blob(processed_ims,targets)
    imgs=torch.from_numpy(imgs)


    # img=imgs[0].numpy()

    # channel_swap = (1, 2, 0)
    # img= img.transpose(channel_swap)
    # h,w=img.shape[:2]
    # for j in range(len(targets[0])):
    #     bbox=[targets[0][j][i] for i in range(4) ]
    #     print (int(bbox[0]*w),int(bbox[1]*h),int(bbox[2]*w),int(bbox[3]*h))
    #     cv2.rectangle(img,(int(bbox[0]*w),int(bbox[1]*h)),(int(bbox[2]*w),int(bbox[3]*h)),(255,255,0),2)
    #     cv2.imwrite(str(j)+'_'+str(int(bbox[0]*w))+'_'+str(int(bbox[1]*h))+'_'+str(int(bbox[2]*w))+'_'+str(int(bbox[3]*h))+'_'+'img_hahah_1.png',img)


    # print (len(imgs),'anno',targets)
    # print (imgs[0])
   

    return (torch.stack(imgs, 0), targets)

from lib.utils.data_augment import preproc
import torch.utils.data as data

def load_data(cfg, phase):
    global target_sizes
    global max_size

    target_sizes=cfg.TARGET_SIZES
    max_size=cfg.MAX_SIZE

    if phase == 'train':
        dataset = dataset_map[cfg.DATASET](cfg.DATASET_DIR, cfg.TRAIN_SETS, preproc(cfg.PIXEL_MEANS, cfg.PROB))

        data_loader = data.DataLoader(dataset, cfg.TRAIN_BATCH_SIZE, num_workers=12,
                                  shuffle=True, collate_fn=detection_collate, pin_memory=True)
    if phase == 'eval':
        dataset = dataset_map[cfg.DATASET](cfg.DATASET_DIR, cfg.TEST_SETS, preproc(cfg.PIXEL_MEANS, -1))
        data_loader = data.DataLoader(dataset, cfg.TEST_BATCH_SIZE, num_workers=cfg.NUM_WORKERS,
                                  shuffle=False, collate_fn=detection_collate, pin_memory=True)
    if phase == 'test':
        dataset = dataset_map[cfg.DATASET](cfg.DATASET_DIR, cfg.TEST_SETS, preproc(cfg.PIXEL_MEANS, -2))
        data_loader = data.DataLoader(dataset, cfg.TEST_BATCH_SIZE, num_workers=cfg.NUM_WORKERS,
                                  shuffle=False, collate_fn=detection_collate, pin_memory=True)
    if phase == 'visualize':
        dataset = dataset_map[cfg.DATASET](cfg.DATASET_DIR, cfg.TEST_SETS, preproc(cfg.PIXEL_MEANS, 1))
        data_loader = data.DataLoader(dataset, cfg.TEST_BATCH_SIZE, num_workers=cfg.NUM_WORKERS,
                                  shuffle=False, collate_fn=detection_collate, pin_memory=True)
    return data_loader
